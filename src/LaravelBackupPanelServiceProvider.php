<?php

namespace EnactOn\ProCashBee\AdminFace;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use PavelMironchik\LaravelBackupPanel\LaravelBackupPanelApplicationServiceProvider;
use PavelMironchik\LaravelBackupPanel\LaravelBackupPanel;


class LaravelBackupPanelServiceProvider extends ServiceProvider
{


    protected function configureBackupAuthorization()
    {

        // Use custom auth instead of Gate expected by Package
        LaravelBackupPanel::auth(function ($request) {
            return app()->environment('local') ||
                   Auth::guard('admin')->check();
        });
    }

    public function boot()
    {
        $this->configureBackupAuthorization();

        if( class_exists('Studio\\Totem\\Totem') )
        \Studio\Totem\Totem::auth( function($request) {
            return app()->environment('local') ||
                   Auth::guard('admin')->check();
        } );

        Gate::define("view-mailweb", function ($user) {
            return true;
        });
    }
}
