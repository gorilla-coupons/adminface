<?php

namespace EnactOn\ProCashBee\AdminFace;

use Davidhsianturi\Compass\CompassServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class CompassAuthProvider extends CompassServiceProvider
{

    public function boot()
    {

        parent::boot();

        $this->registerCustomRoutes();
    }

    protected function registerCustomRoutes()
    {
        $vendor_dir = base_path('/vendor/davidhsianturi/laravel-compass/src');
        Route::namespace('Davidhsianturi\Compass\Http\Controllers')
            ->as('compass.')
            ->prefix(config('compass.path'))
            ->middleware(['web','auth:admin'])
            ->group(function () use($vendor_dir) {
                $this->loadRoutesFrom(  $vendor_dir .'/Http/routes.php');
            });
    }
}
