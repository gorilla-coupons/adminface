<?php

namespace EnactOn\ProCashBee\AdminFace;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;


class AdminFaceProvider extends ServiceProvider
{

    public function boot()
    {

        Route::get( env('ADMINFACE_URL','adminface') , '\EnactOn\ProCashBee\AdminFace\AdminFaceController@auth' )->middleware('web');


        Auth::viaRequest('admin-face', function ($request) {

            if( app()->environment('local') )
                return AdminUser::first();
            else
            return AdminUser::find($request->session()->get('adminface_id')  );
        });
    }

}
