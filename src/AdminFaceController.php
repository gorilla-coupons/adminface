<?php
namespace EnactOn\ProCashBee\AdminFace;

use  App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class AdminFaceController extends Controller
{
    //

    public function auth(Request $request)
    {
        $admin_user = DB::table('tb_users')->where('username', $request->user)->first();

        if (!$admin_user) return false;

        if (Hash::check((base64_encode($admin_user->password)), $request->token) && Hash::check($request->time, $request->secret)) {
            $request->session()->put('adminface_expiry', time() + (60 * 30));
            $request->session()->put('adminface_id', $admin_user->id);
            return redirect($request->route_to);
        }
        return false;
    }
}
